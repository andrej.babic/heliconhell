import base64
import struct

to_convert = "NAkAALoLAAA="

packed_data = base64.b64decode(to_convert.encode("ascii"))

temperature, humidity = struct.unpack_from("ff", packed_data)
print("Temperature: ", temperature)
print("Humidity: ", humidity)
