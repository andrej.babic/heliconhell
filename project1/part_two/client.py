import binascii
import socket
import struct
import time
import random

HOST = "localhost"
PORT = 50005


def gen_values() -> tuple:
    humidity = round(random.uniform(0.0, 100.0), 2)
    temperature = round(random.uniform(-50.0, 120.0), 2)
    return humidity, temperature


def run():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))

    while True:
        humidity, temperature = gen_values()
        packed_data = struct.pack("ff", humidity, temperature)
        hex_value = binascii.hexlify(packed_data)

        print("Sending: ", humidity, temperature)
        s.sendall(hex_value)

        time.sleep(10)

    s.close()


if __name__ == "__main__":
    run()
