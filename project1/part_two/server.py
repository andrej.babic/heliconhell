import base64
import binascii
import socket
import struct

HOST = ""
PORT = 50005


def parse_data(data: str) -> tuple:
    try:
        packed_data = binascii.unhexlify(data)
        temperature, humidity = struct.unpack_from("ff", packed_data)
        return temperature, humidity
    except:
        raise Exception("Malformatted data received: {}".format(data))


def run():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((HOST, PORT))
    s.listen(1)
    conn, addr = s.accept()

    while True:
        data = conn.recv(1024)
        if not data:
            break

        try:
            temperature, humidity = parse_data(data)
            print("Temperature: {:.2f}".format(temperature))
            print("Humidity: {:.2f}".format(humidity))
        except Exception as e:
            print(e)
    conn.close()


if __name__ == "__main__":
    run()
