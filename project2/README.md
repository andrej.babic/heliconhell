# Heliconhell

## Quickstart

```
$ make docker-up
$ docker exec heliconhell_app python heliconhell/manage.py createsuperuser
```


## Run tests

```
$ make test
```
