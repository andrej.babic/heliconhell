#!/bin/bash

set -eu

if [ "$1" = 'runproduction' ]; then
	python /app/manage.py migrate
	python /app/manage.py collectstatic --noinput
	exec gunicorn app.wsgi -b 0.0.0.0:8000
elif [ "$1" = 'runserver' ]; then
	python /app/manage.py migrate
	exec python /app/manage.py runserver 0.0.0.0:8000
elif [ "$1" = 'runqueue_worker' ]; then
	exec celery -A heliconhell worker -l INFO
elif [ "$1" = 'test' ]; then
	exec python /app/manage.py test
fi

exec $@
