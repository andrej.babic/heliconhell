from django.core.exceptions import ValidationError
from django.test import TestCase
from django.db.utils import IntegrityError
from sensordata import models


class LocationModelTests(TestCase):
    def test_location_slug(self):
        name = "Stockholm 1 A"
        expected_slug = "stockholm-1-a"

        obj = models.Location.objects.create(name=name)
        self.assertEqual(obj.slug, expected_slug)

    def test_location_name_too_short(self):
        name = "a"  # min len: 2

        with self.assertRaises(ValidationError):
            obj = models.Location(name=name)
            obj.full_clean()

    def test_location_name_too_long(self):
        name = "stockholm london paris berlin rome madrid"  # max len: 40
        with self.assertRaises(ValidationError):
            obj = models.Location(name=name)
            obj.full_clean()


class SensorModelTests(TestCase):
    def setUp(self):
        self.location = models.Location.objects.create(name="Location 1")

    def test_optional_description(self):
        name = "Stockholm 1 A"

        # no description provided, should not raise error
        obj = models.Sensor(name=name, location=self.location)
        obj.full_clean()
        obj.save()

    def test_sensor_name_too_short(self):
        name = "a"  # min len: 2

        with self.assertRaises(ValidationError):
            obj = models.Sensor(name=name, location=self.location)
            obj.full_clean()
            obj.save()

    def test_sensor_name_too_long(self):
        name = "stockholm london paris berlin rome madrid"  # max len: 40
        with self.assertRaises(ValidationError):
            obj = models.Sensor(name=name, location=self.location)
            obj.full_clean()
            obj.save()

    def test_unique_sensor_name_per_location(self):
        name = "Stockholm"
        models.Sensor.objects.create(name=name, location=self.location)

        with self.assertRaises(IntegrityError):
            models.Sensor.objects.create(name=name, location=self.location)
