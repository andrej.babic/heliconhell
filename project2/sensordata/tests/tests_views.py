import json
import uuid
from django.test import Client, TestCase
from django.urls import reverse
from sensordata import models


class LocationViewTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_location_list(self):
        models.Location.objects.create(name="loc 1")
        models.Location.objects.create(name="loc 2")

        response = self.client.get(reverse("api:list_locations"))
        res_data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(res_data), 2)

    def test_location_post(self):
        data = {"doesnt-exist": "loc 1"}
        response = self.client.post(
            reverse("api:create_location"),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 422)

        data = {"name": "loc 1"}
        response = self.client.post(
            reverse("api:create_location"),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content).get("name", None), data["name"])

    def test_location_get(self):
        response = self.client.get(
            reverse("api:get_location", kwargs={"location_id": str(uuid.uuid4())})
        )
        self.assertEqual(response.status_code, 404)

        obj = models.Location.objects.create(name="loc 1")
        id = str(obj.id)

        response = self.client.get(
            reverse("api:get_location", kwargs={"location_id": id})
        )
        res_data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(res_data["id"], id)
        self.assertEqual(res_data["name"], obj.name)

    def test_location_update(self):
        data = {"name": "name"}
        response = self.client.put(
            reverse("api:update_location", kwargs={"location_id": str(uuid.uuid4())}),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

        obj = models.Location.objects.create(name="loc 1")
        id = str(obj.id)

        data = {"doesnt-exist": "name"}
        response = self.client.put(
            reverse("api:update_location", kwargs={"location_id": id}),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 422)

        data = {"name": "loc 2"}
        response = self.client.put(
            reverse("api:update_location", kwargs={"location_id": id}),
            json.dumps(data),
            content_type="application/json",
        )
        res_data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(res_data["id"], id)
        self.assertEqual(res_data["name"], data["name"])

    def test_location_delete(self):
        response = self.client.delete(
            reverse("api:delete_location", kwargs={"location_id": str(uuid.uuid4())})
        )
        self.assertEqual(response.status_code, 404)

        obj = models.Location.objects.create(name="loc 1")
        id = str(obj.id)

        response = self.client.delete(
            reverse("api:delete_location", kwargs={"location_id": id})
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(models.Location.objects.filter(id=obj.id).count(), 0)


class SensorViewTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.location_1 = models.Location.objects.create(name="loc 1")
        self.location_2 = models.Location.objects.create(name="loc 2")

    def test_sensor_list(self):
        models.Sensor.objects.create(location=self.location_1, name="sen 1")
        models.Sensor.objects.create(location=self.location_2, name="sen 2")

        response = self.client.get(reverse("api:list_sensors"))
        res_data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(res_data), 2)

    def test_sensor_post(self):
        data = {"nothing": None}
        response = self.client.post(
            reverse("api:create_sensor"),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 422)

        data = {"location_id": str(self.location_1.id), "name": "sen 1"}
        response = self.client.post(
            reverse("api:create_sensor"),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content).get("name", None), data["name"])

    def test_sensor_get(self):
        response = self.client.get(
            reverse("api:get_sensor", kwargs={"sensor_id": str(uuid.uuid4())})
        )
        self.assertEqual(response.status_code, 404)

        obj = models.Sensor.objects.create(
            location=self.location_1, name="sen 1", description="test"
        )
        id = str(obj.id)

        response = self.client.get(reverse("api:get_sensor", kwargs={"sensor_id": id}))
        res_data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(res_data["name"], obj.name)
        self.assertEqual(res_data["description"], obj.description)

    def test_sensor_update(self):
        data = {"location_id": str(self.location_1.id), "name": "name"}
        response = self.client.put(
            reverse("api:update_sensor", kwargs={"sensor_id": str(uuid.uuid4())}),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 404)

        obj = models.Sensor.objects.create(location=self.location_1, name="sen 1")
        id = str(obj.id)

        data = {"doesnt-exist": "name"}
        response = self.client.put(
            reverse("api:update_sensor", kwargs={"sensor_id": id}),
            json.dumps(data),
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 422)

        data = {"location_id": str(self.location_1.id), "name": "sen 2"}
        expected_location = {
            "id": str(self.location_1.id),
            "name": self.location_1.name,
            "slug": self.location_1.slug,
        }
        response = self.client.put(
            reverse("api:update_sensor", kwargs={"sensor_id": id}),
            json.dumps(data),
            content_type="application/json",
        )
        res_data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(res_data["id"], id)
        self.assertEqual(res_data["name"], data["name"])
        self.assertEqual(res_data["location"], expected_location)

    def test_sensor_delete(self):
        response = self.client.delete(
            reverse("api:delete_sensor", kwargs={"sensor_id": str(uuid.uuid4())})
        )
        self.assertEqual(response.status_code, 404)

        obj = models.Sensor.objects.create(location=self.location_1, name="sen 1")
        id = str(obj.id)

        response = self.client.delete(
            reverse("api:delete_sensor", kwargs={"sensor_id": id})
        )
        self.assertEqual(response.status_code, 204)
        self.assertEqual(models.Sensor.objects.filter(id=obj.id).count(), 0)
