import uuid
from django.http import HttpRequest, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404
from ninja import Router, Query
from typing import List
from sensordata import models, schemas


location_router = Router()


@location_router.get("", response=List[schemas.LocationOut])
def list_locations(request):
    return models.Location.objects.all()


@location_router.post("", response=schemas.LocationOut)
def create_location(request, payload: schemas.LocationIn):
    return models.Location.objects.create(**payload.dict())


@location_router.get("/{location_id}", response=schemas.LocationOut)
def get_location(request, location_id: uuid.UUID):
    return get_object_or_404(models.Location, id=location_id)


@location_router.put("/{location_id}", response=schemas.LocationOut)
def update_location(request, location_id: uuid.UUID, payload: schemas.LocationIn):
    location = get_object_or_404(models.Location, id=location_id)
    for attr, value in payload.dict().items():
        setattr(location, attr, value)
    location.save()
    return location


@location_router.delete("/{location_id}")
def delete_location(
    request: HttpRequest, response: HttpResponse, location_id: uuid.UUID
):
    location = get_object_or_404(models.Location, id=location_id)
    location.delete()
    return HttpResponse(status=204)


sensor_router = Router()


@sensor_router.get("", response=List[schemas.SensorOut])
def list_sensors(request, filters: schemas.SensorFilter = Query(...)):
    sensors = models.Sensor.objects.all()
    sensors = filters.filter(sensors)
    return sensors


@sensor_router.post("", response=schemas.SensorOut)
def create_sensor(request, payload: schemas.SensorIn):
    return models.Sensor.objects.create(**payload.dict())


@sensor_router.get("/{sensor_id}", response=schemas.SensorOut)
def get_sensor(request, sensor_id: uuid.UUID):
    return get_object_or_404(models.Sensor, id=sensor_id)


@sensor_router.put("/{sensor_id}", response=schemas.SensorOut)
def update_sensor(request, sensor_id: uuid.UUID, payload: schemas.SensorIn):
    sensor = get_object_or_404(models.Sensor, id=sensor_id)
    data = payload.dict()

    # Update location
    location_id = data.pop("location_id", None)
    location = get_object_or_404(models.Location, id=location_id)
    sensor.location = location

    for attr, value in data.items():
        setattr(sensor, attr, value)
    sensor.save()
    return sensor


@sensor_router.delete("/{sensor_id}")
def delete_sensor(request: HttpRequest, response: HttpResponse, sensor_id: uuid.UUID):
    sensor = get_object_or_404(models.Sensor, id=sensor_id)
    sensor.delete()
    return HttpResponse(status=204)
