import uuid
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils.text import slugify


class Location(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=40, validators=[MinLengthValidator(2)])
    slug = models.SlugField(editable=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Location, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Sensor(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    name = models.CharField(max_length=40, validators=[MinLengthValidator(2)])
    description = models.CharField(null=True, blank=True)

    class Meta:
        ordering = ["name"]
        constraints = [
            models.UniqueConstraint(
                fields=["location", "name"], name="unique_name_per_location"
            )
        ]

    def __str__(self):
        return self.name
