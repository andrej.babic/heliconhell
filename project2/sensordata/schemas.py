from ninja import FilterSchema, Schema
from uuid import UUID


class LocationIn(Schema):
    name: str


class LocationOut(Schema):
    id: UUID
    name: str
    slug: str


class SensorFilter(FilterSchema):
    location: UUID | None = None


class SensorIn(Schema):
    location_id: UUID
    name: str
    description: str | None = None


class SensorOut(Schema):
    id: UUID
    location: LocationOut
    name: str
    description: str | None = None
