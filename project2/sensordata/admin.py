from django.contrib import admin
from sensordata import models


class LocationAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "slug",
    ]


class SensorAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "location",
    ]
    sortable_by = [
        "name",
        "location",
    ]
    list_filter = [
        "location__name",
    ]


admin.site.register(models.Location, LocationAdmin)
admin.site.register(models.Sensor, SensorAdmin)
