from django.http import JsonResponse
from ninja import NinjaAPI
from ninja.errors import ValidationError
from sensordata.views import location_router, sensor_router

api = NinjaAPI(urls_namespace="api")


@api.exception_handler(ValidationError)
def validation_errors(request, exc):
    return JsonResponse(dict(errors=exc.errors), safe=False, status=422)


@api.exception_handler(Exception)
def generic_errors(request, exc):
    return JsonResponse(dict(errors=exc.message_dict), safe=False, status=422)


api.add_router("locations", location_router, tags=["locations"])
api.add_router("sensors", sensor_router, tags=["sensors"])
